/*
 * The Typescript compiler will call our createElement() function to create the
 * nodes.  Unlike React.js, we're not maintaining and diffing a virtual DOM. We
 * simply create the required nodes directly.  In many cases, this is all you
 * need.
 *
 * As we're watching state changes with enough specificity to know which
 * components have changed state, we can avoid full diff re-renders, and only
 * update those components that have changed state, and then compare the
 * regenerated fragment to the existing DOM.
 *
 * Also, we avoid passing around props and lifting up state refactors by using a
 * central state store, which is the most common React.js pattern anyway.
 */

let components:any = []; // component stack

/* convert a html property like 'onClick' to the real event name like 'click'
 * Alternatively, could just add these raw events to the DetailedHTMLProps type */
interface StringArray {
	[index: string]: string;
}
const eventDict: StringArray = {
	'onClick': 'click',
	'onKeyPress': 'keypress'
}
function realEvent(key: string) {
	return eventDict[key];
}

/*
 * type: can be either a tag name string (such as 'div' or 'span'), a React
 * component type (a class or a function), or a React fragment type.
 */

interface ICreateElement {
	(type: any, config?: { [index: string]: any; }, ...children: any): any
}
const createElement: ICreateElement =
	(type: any, config: {[index: string]:any} = {}, ...children: any) => {
	switch (typeof type) {
		case 'function': {		// Component
			const t = new type(config);
			t.props = (config) ? config : {};
			components.push(t);
			return t.render();
		}
		case 'string':			// eg. 'div' => use to create element
			break;
		default:				// object
			if (Array.isArray(type)) {
				let child: any;
				while ((child = type.shift()) !== undefined) {
					render(child, config.parentNode, true);
				}
			}
			return type;
	}
	return createNode(type, config, children);
}

function createNode(type: string, config: {[index: string]:any} = {}, children: any) {
	const element = document.createElement(type);
	for (const key in config) { // apply attributes
		if (key === 'className' && config[key] !== undefined)
			element.classList.add(...config[key].split(' '));
		else { // apply non class attributes
			if (typeof config[key] === 'function')
				element.addEventListener(realEvent(key), config[key]);
			else
				element.setAttribute(key, config[key]);
		}
	}
	if (config && config.innerHTML !== undefined) element.innerHTML = config.innerHTML;
	if (children) { // child nodes
		if (!Array.isArray(children)) children = [ children ];
		children.forEach((child: any) => {
			if (typeof child === 'string')
				child = document.createTextNode(String(child));
			else
				child = createElement(child, { parentNode: element }, []);
			if (child instanceof Node) element.appendChild(child);
		});
	}
	return element;
}

function componentDidMount(component: any) {
	if (component.componentDidMount !== undefined) component.componentDidMount();
}

function render(element: any, parentNode?: HTMLElement|(Node & ParentNode)|null|undefined, isChild: boolean = false) {
	if (!element) throw new Error('missing element to render');
	(parentNode) || (parentNode = document.getElementById('root'));
	if (!parentNode || parentNode === undefined) throw new Error('Unable to find root node');

	if (!isChild && parentNode.childElementCount) {
		// this is a root node with children, so test before append/replace
		if (element.id !== undefined) {
			const oldNode = document.getElementById(element.id);
			if (!oldNode) parentNode.appendChild(element);		// new element, append
			else {
				if (oldNode.isEqualNode(element)) return;		// node same, skip update
				if (oldNode.parentNode)							// node different, replace
					oldNode.parentNode.replaceChild(element, oldNode);
			}
		}
		else { // no id, compare all children, skip if match found
			for (let i = 0, ii = parentNode.children.length; i < ii; i++) {
				if (parentNode.children[i].isEqualNode(element)) return;
			}
		}
	}
	else { // child node or parent with no children, append without checking
		parentNode.appendChild(element);
	}

	// on root node, set back refs and events for component objects
	if (!isChild) {
		let component: any;
		while (component = components.shift()) {
			if (component.id) {
				component.node = document.getElementById(component.id);
				component.parentNode = parentNode;
			}
			componentDidMount(component);
		}
	}
}
render.key = 0; // key for each instance of a Component

export {
	createElement,
	render
}
